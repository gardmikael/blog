@extends('main')

@section('title', '| View Post')

@section('stylesheets')

	{!! Html::style('css/app.css') !!}

@endsection

@section('content')
	
	<div class="row">

		<div class="col-md-8">
		
			<h1>{{ $post->title }}</h1>

			<p class="lead">{{ $post->body }}</p>
		</div>

		<div class="col-md-4">
			<div class="well">
				<label>Slug:</label>
				<p> <a href="{{ route('blog.single', $post->slug) }}"> {{ route('blog.single', $post->slug) }}</a></p>

				<label>Opprettet:</label>
				<p>{{ date('j. M - Y H:i', strtotime($post->created_at)) }}</p>

				<label>Oppdatert:</label>
				<p>{{ date('j. M - Y H:i', strtotime($post->updated_at)) }}</p>

				<hr>
				<div class="row">
					<div class="col-sm-6">
						{!! Html::linkRoute('posts.edit', 'Endre', [$post->id], ['class' => "btn btn-primary btn-block"] )!!}
					
					</div>
					<div class="col-sm-6" >
						{!! Form::open(array('route' => ['posts.destroy', $post->id], 'method' => 'delete')) !!}

						{!! Form::submit("Slett", ['class' => "btn btn-danger btn-block"]) !!}
						
						{!! Form::close() !!}
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						{{ Html::linkRoute('posts.index', '<< Tilbake', [], ['class' => 'btn btn-default btn-block btn-marg']) }}
					</div>
				</div>
			</div>
			
		</div>
	</div>
@endsection