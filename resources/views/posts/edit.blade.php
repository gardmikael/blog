@extends('main')

@section('title', '| Rediger Innlegg')

@section('stylesheets')

	{!! Html::style('css/app.css') !!}

@endsection

@section('content')
	
	<div class="row">
		{!! Form::model($post, ['route' => ['posts.update', $post->id], 'method' => 'PUT'] ) !!}
		<div class="col-md-8">
			{{ Form::label('title', 'Tittel') }}			
			{{ Form::text('title', null, ['class' => 'form-control input-lg']) }}
			{{ Form::label('body', 'Innlegg') }}
			{{ Form::textarea('body', null, ['class' => 'form-control']) }}
			
		</div>

		<div class="col-md-4">
			<div class="well">
				<label>Opprettet:</label>
				<p>{{ date('j. M - Y H:i', strtotime($post->created_at)) }}</p>

				<label>Oppdatert:</label>
				<p>{{ date('j. M - Y H:i', strtotime($post->updated_at)) }}</p>

				<hr>
				<div class="row">
					<div class="col-sm-6">
						{!! Html::linkRoute('posts.show', 'Avbryt', [$post->id], ['class' => "btn btn-danger btn-block"] )!!}
					
					</div>
					<div class="col-sm-6" >
						{{ Form::submit('Lagre', ['class' => 'btn btn-success btn-block']) }}
					</div>
				</div>
			</div>
		</div>
		{!! Form::close() !!}	
	</div>
@endsection