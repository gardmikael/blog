@extends('main')

@section('title', '| Index')

@section('content')

	<div class="row">
		<div class="col-md-9">
			<h1>Alle innlegg</h1>
		</div>

		<div class="col-md-3">
			<a href="{{ route('posts.create') }}" class="btn btn-lg btn-block btn-primary btn-marg">Opprett nytt innlegg</a>
		</div>
		<div class="col-md-12">
			<hr>	
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<table class="table table-hover">
				<thead>
					<th>#</th>
					<th>Tittel</th>
					<th>Tekst</th>
					<th>Opprettet</th>
					<th></th>
				</thead>
				
				<tbody>
					@foreach ($posts as $post)
					<tr>
						<th>{{ $post->id }}</th>
						<td>{{ $post->title }}</td>
						<td>{{ substr($post->body, 0, 50) }} {{ strlen($post->body) > 50 ? "..." : "" }}</td>
						<td>{{ date('j. M - Y H:i', strtotime($post->created_at)) }}</td>
						<td><a href="{{ route('posts.show', $post->id) }}" class="btn btn-default btn-sm">View</a>
						<a href="{{ route('posts.edit', $post->id) }}" class="btn btn-default btn-sm">Edit</a></td>

					</tr>

					@endforeach
				</tbody>
			</table>
			<div class="text-center">
				{!! $posts->links(); !!}
			</div>
		</div>
	</div>
		
	
@endsection